module.exports = {             
    sessionSecret : 'tokune',
    dev_mode      : true,
    title         : 'Poptile',
    port          : 3000,
    mysql         : {
        database : "notes",
        protocol : "mysql",
        host     : "127.0.0.1",
        user     : "root",
        password : "tokune",
        query    :{
            pool : true,
        }
    }
}
