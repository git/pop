var config = require('../config');
var async = require('async');  

exports.route = function(app) {
    app.get('/', function(req, res) {
        res.render('main', {title: config.title});
    });
    app.get('/index', function(req, res) {
        res.render('index');
    });
    app.get('/game', function(req, res) {
        res.render('game');
    });
};
